# type: ignore
import pytest
from pony.orm.core import ObjectNotFound

from controller.controller import (
    create_todo, get_todo, get_todos, update_todo,
    delete_todo
)
from entity.schema import TodoInput, TodoSchema as TodoSchema


class TestCreateTodo:
    def test_create_todo(self):
        todo_input = TodoInput(
            title="Test Todo",
            description="Test Description"
        )

        todo = create_todo(todo_input)
        assert isinstance(todo, TodoSchema)
        assert todo.title == "Test Todo"
        assert todo.description == "Test Description"
        assert todo.completed is not None
        assert todo.id is not None


class TestGetTodo:
    def test_get_todo(self):
        todo_input = TodoInput(
            title="Test Todo",
            description="Test Description"
        )

        todo = create_todo(todo_input)

        todo = get_todo(todo.id)
        assert isinstance(todo, TodoSchema)
        assert todo.title == "Test Todo"
        assert todo.description == "Test Description"
        assert todo.completed is not None
        assert todo.id is not None

    def test_get_todo_not_found(self):
        with pytest.raises(ObjectNotFound):
            get_todo(999)


class TestGetTodos:
    def test_get_todos(self):
        create_todo(
            TodoInput(
                title="Test Todo",
                description="Test Description"
            )
        )
        create_todo(
            TodoInput(
                title="Test Todo 2",
                description="Test Description 2"
            )
        )
        todos = get_todos()
        assert isinstance(todos, list)
        assert len(todos) == 2
        for todo in todos:
            assert isinstance(todo, TodoSchema)


class TestUpdateTodo:
    def test_update_todo(self):
        todo_input = TodoInput(
            title="Test Todo",
            description="Test Description"
        )

        todo = create_todo(todo_input)

        todo.completed = True
        todo.title = "Test Todo Updated"
        todo.description = "Test Description Updated"

        updated_todo = update_todo(todo.id, todo)
        assert isinstance(updated_todo, TodoSchema)
        assert updated_todo.title == "Test Todo Updated"
        assert updated_todo.description == "Test Description Updated"
        assert updated_todo.completed is True
        assert updated_todo.id is not None

    def test_update_todo_not_found(self):
        todo = TodoInput(
            title="Test Todo",
            description="Test Description",
            completed=False
        )

        with pytest.raises(ObjectNotFound):
            update_todo(999, todo)


class TestDeleteTodo:
    def test_delete_todo(self):
        todo_input = TodoInput(
            title="Test Todo",
            description="Test Description"
        )

        todo = create_todo(todo_input)

        assert delete_todo(todo.id) is True

    def test_delete_todo_not_found(self):
        with pytest.raises(ObjectNotFound):
            delete_todo(999)
