# type: ignore

class TestRouterCreateTodo:
    def test_create_todo(self, client):
        response = client.post(
            "/todos/",
            json={"title": "test", "description": "test"},
        )
        assert response.status_code == 201
        assert response.json == {
            "id": 1,
            "title": "test",
            "description": "test",
            "completed": False,
        }

    def test_create_todo_invalid(self, client, mocker):

        mocker.patch(
            "controller.controller.create_todo",
            side_effect=Exception("Test Error"),
        )

        response = client.post("/todos/", json={})
        assert response.status_code == 400


class TestRouterGetTodo:
    def test_get_todo(self, client):
        response = client.post(
            "/todos/",
            json={"title": "test", "description": "test"},
        )
        todo_id = response.json["id"]

        response = client.get(f"/todos/{todo_id}")
        assert response.status_code == 200
        assert response.json == {
            "id": todo_id,
            "title": "test",
            "description": "test",
            "completed": False,
        }

    def test_get_todo_not_found(self, client):
        response = client.get("/todos/999")
        assert response.status_code == 404

    def test_get_todo_invalid(self, client, mocker):
        mocker.patch(
            "boundary.router.get_todo",
            side_effect=Exception("Test Error"),
        )

        response = client.get("/todos/1")
        assert response.status_code == 400


class TestRouterGetTodos:
    def test_get_todos(self, client):
        response = client.get("/todos/")
        assert response.status_code == 200
        assert response.json == []

        response = client.post(
            "/todos/",
            json={"title": "test", "description": "test"},
        )
        todo_id = response.json["id"]

        response = client.get("/todos/")
        assert response.status_code == 200
        assert response.json == [
            {
                "id": todo_id,
                "title": "test",
                "description": "test",
                "completed": False,
            }
        ]

    def test_get_todos_invalid(self, client, mocker):
        mocker.patch(
            "boundary.router.get_todos",
            side_effect=Exception("Test Error"),
        )

        response = client.get("/todos/")
        assert response.status_code == 400


class TestRouterUpdateTodo:
    def test_update_todo(self, client):
        response = client.post(
            "/todos/",
            json={"title": "test", "description": "test"},
        )
        todo_id = response.json["id"]

        response = client.put(
            f"/todos/{todo_id}",
            json={"title": "test updated", "description": "test updated"},
        )
        assert response.status_code == 200
        assert response.json == {
            "id": todo_id,
            "title": "test updated",
            "description": "test updated",
            "completed": False,
        }

    def test_update_todo_not_found(self, client):
        response = client.put(
            "/todos/999",
            json={"title": "test updated", "description": "test updated"}
        )
        assert response.status_code == 404

    def test_update_todo_invalid(self, client, mocker):
        mocker.patch(
            "boundary.router.update_todo",
            side_effect=Exception("Test Error"),
        )

        response = client.put("/todos/1", json={})
        assert response.status_code == 400


class TestRouterDeleteTodo:
    def test_delete_todo(self, client):
        response = client.post(
            "/todos/",
            json={"title": "test", "description": "test"},
        )
        todo_id = response.json["id"]

        response = client.delete(f"/todos/{todo_id}")
        assert response.status_code == 204

    def test_delete_todo_not_found(self, client):
        response = client.delete("/todos/999")
        assert response.status_code == 404

    def test_delete_todo_invalid(self, client, mocker):
        mocker.patch(
            "boundary.router.delete_todo",
            side_effect=Exception("Test Error"),
        )

        response = client.delete("/todos/1")
        assert response.status_code == 400
