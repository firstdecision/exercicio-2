# type: ignore
from os import environ

import pytest
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from app import app


def pytest_configure(config):
    """
    Allows plugins and conftest files to perform initial configuration.
    This hook is called for every plugin and initial conftest
    file after command line options have been parsed.
    """

    db_test_string = environ["DB_DATABASE"] + '_test'

    conn = psycopg2.connect(
        dbname=environ["DB_DATABASE"],
        user=environ["DB_USER"],
        password=environ["DB_PASSWORD"],
        host=environ["DB_HOST"],
        port=environ["DB_PORT"]
    )

    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    with conn.cursor() as cursor:
        cursor.execute(
            f"DROP DATABASE IF EXISTS {db_test_string} WITH (FORCE)"
        )
        cursor.execute(f"CREATE DATABASE {db_test_string}")

    environ['DB_DATABASE'] = f'{environ["DB_DATABASE"]}_test'


def pytest_unconfigure(config):
    """
    Allows plugins and conftest files to perform initial configuration.
    This hook is called for every plugin and initial conftest
    file after command line options have been parsed.
    """

    if '_test' in environ["DB_DATABASE"]:
        db_test_string = environ["DB_DATABASE"]
        environ["DB_DATABASE"] = environ["DB_DATABASE"].replace('_test', '')
    else:
        db_test_string = environ["DB_DATABASE"] + '_test'

    conn = psycopg2.connect(
        dbname=environ["DB_DATABASE"],
        user=environ["DB_USER"],
        password=environ["DB_PASSWORD"],
        host=environ["DB_HOST"],
        port=environ["DB_PORT"]
    )

    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    with conn.cursor() as cursor:
        cursor.execute(f"DROP DATABASE {db_test_string} WITH (FORCE)")


@pytest.fixture(scope="function", autouse=True)
def renew_database(request):

    from entity.models import drop_tables, create_tables

    create_tables()

    request.addfinalizer(drop_tables)


@pytest.fixture(scope="function", autouse=False)
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client
