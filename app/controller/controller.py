from typing import List

from pony.orm import db_session
from pydantic import TypeAdapter

from entity.models import Todo
from entity.schema import TodoInput, TodoSchema as TodoSchema


def create_todo(todo_input: TodoInput) -> TodoSchema:
    """Create a new todo item in the database
    and return the created todo item."""
    with db_session:
        todo = Todo(**todo_input.model_dump())

    todo_output = TodoSchema(**todo.to_dict())
    return todo_output


def get_todo(id: int) -> TodoSchema:
    """Get a todo item from the database by id."""
    with db_session:
        todo = Todo[id]  # type: ignore

    todo_output = TodoSchema(**todo.to_dict())
    return todo_output


def get_todos() -> List[dict]:
    """Get all todo items from the database."""
    with db_session:
        todos = Todo.select()[:]
        todos_dict = [todo.to_dict() for todo in todos]

    todos_adapter = (
        TypeAdapter(List[TodoSchema])
        .validate_python(todos_dict)
    )

    todos_output = [todo.model_dump() for todo in todos_adapter]
    return todos_output


def update_todo(id: int, todo_input: TodoInput) -> TodoSchema:
    """Update a todo item in the database."""
    with db_session:
        todo = Todo[id]  # type: ignore
        todo.set(**todo_input.model_dump())

    todo_output = TodoSchema(**todo.to_dict())
    return todo_output


def delete_todo(id: int) -> bool:
    """Delete a todo item from the database."""
    with db_session:
        todo = Todo[id]  # type: ignore
        todo.delete()

    return True
