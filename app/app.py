from flask import Flask

from boundary.router import router

app = Flask(__name__)
app.register_blueprint(router, url_prefix='/todos')
