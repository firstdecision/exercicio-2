from typing import Optional
from pydantic import BaseModel


class TodoInput(BaseModel):
    title: str
    description: str
    completed: Optional[bool] = False


class TodoSchema(BaseModel):
    id: int
    title: str
    description: str
    completed: bool
