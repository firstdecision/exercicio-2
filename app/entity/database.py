from os import environ

from pony.orm import Database

db = Database()

db.bind(
    provider='postgres',
    database=environ["DB_DATABASE"],
    user=environ["DB_USER"],
    password=environ["DB_PASSWORD"],
    host=environ["DB_HOST"],
    port=environ["DB_PORT"]
)
