from entity.database import db
from pony.orm import Required, Optional, PrimaryKey


class Todo(db.Entity):
    id = PrimaryKey(int, auto=True)
    title = Required(str)
    description = Required(str)
    completed = Optional(bool, default=False)


def create_tables() -> None:
    db.create_tables(check_tables=True)


def drop_tables() -> None:
    db.drop_all_tables(with_all_data=True)


db.generate_mapping(create_tables=True)
create_tables()
