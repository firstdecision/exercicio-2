from typing import List

from flask import Blueprint, jsonify, abort, Response
from flask_pydantic import validate
from pony.orm.core import ObjectNotFound

from controller.controller import (
    create_todo, get_todo, get_todos, update_todo, delete_todo
)
from entity.schema import TodoInput, TodoSchema as TodoSchema


router = Blueprint(
    'Todo API', __name__
)


@router.route('/', methods=['POST'])
@validate(on_success_status=201)
def router_create_todo(body: TodoInput) -> TodoSchema:
    try:
        todo = create_todo(body)
        return todo
    except Exception as e:
        abort(400, description=jsonify(error=str(e)))


@router.route('/<int:id>', methods=['GET'])
@validate()
def router_get_todo(id: int) -> TodoSchema:
    try:
        todo = get_todo(id)
        return todo
    except ObjectNotFound as e:
        abort(404, description=jsonify(error=str(e)))
    except Exception as e:
        abort(400, description=jsonify(error=str(e)))


@router.route('/', methods=['GET'])
@validate()
def router_get_todos() -> List[TodoSchema]:
    try:
        todos = get_todos()
        return todos  # type: ignore  # there is a bug in flask-pydantic
    except Exception as e:
        abort(400, description=jsonify(error=str(e)))


@router.route('/<int:id>', methods=['PUT'])
@validate()
def router_update_todo(id: int, body: TodoInput) -> TodoSchema:
    try:
        todo = update_todo(id, body)
        return todo
    except ObjectNotFound as e:
        abort(404, description=jsonify(error=str(e)))
    except Exception as e:
        abort(400, description=jsonify(error=str(e)))


@router.route('/<int:id>', methods=['DELETE'])
@validate()
def router_delete_todo(id: int) -> Response:
    try:
        delete_todo(id)
        return Response(status=204)
    except ObjectNotFound as e:
        abort(404, description=jsonify(error=str(e)))
    except Exception as e:
        abort(400, description=jsonify(error=str(e)))
