FROM python:3.12.1-slim

RUN pip install pipenv

WORKDIR /app
COPY app/Pipfile .
RUN pipenv install

COPY app/ .

ENTRYPOINT pipenv run flask --app app run --host=0.0.0.0 --port=8002 --reload
